package com.pawelbanasik.singleton;

public class SingletonExample {

	private static SingletonExample instance = null;
	private String name;

	private SingletonExample() {
		System.out.println("[Contruct] Utworzono nową instancję.");
	}

	public static SingletonExample getInstance() {
		// leniwa inicjalizacja
		if (instance == null) {
			instance = new SingletonExample();
		} else {
			System.out.println("Zwracam instancję.");
		}
		return instance;
	}

	public String getName() {
		return name;
	}

	public void setName(String _name) {
		this.name = _name;
	}
}
