package com.pawelbanasik.singleton;

public class SimpleObject {

    public static int index = 0;
    public String name;

    public SimpleObject() {
        index++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
