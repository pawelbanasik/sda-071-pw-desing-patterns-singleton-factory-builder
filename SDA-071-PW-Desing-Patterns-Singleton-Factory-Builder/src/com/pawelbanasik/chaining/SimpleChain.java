package com.pawelbanasik.chaining;

public class SimpleChain {
    private String name;
    private String lastName;

    public String getName() {
        return name;
    }

    public SimpleChain setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public SimpleChain setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }
}
